//{ Driver Code Starts
import java.io.*;
import java.util.*;
class GfG
{
    public static void main(String args[])throws IOException
        {
            Scanner sc = new Scanner(System.in);
            int t = sc.nextInt();
            while(t-->0)
                {
                    int k = sc.nextInt();
                    String str = sc.next();
                    Solution obj = new Solution();
                    System.out.println(obj.findMaximumNum(str, k));
                }
        }
}
// } Driver Code Ends


class Solution{
    static String ans;
    
    public static String swap(String s, int i, int j) {
    char[] charArray = s.toCharArray();
    char temp = charArray[i];
    charArray[i] = charArray[j];
    charArray[j] = temp;
    return new String(charArray);
    }
     
    public static void solve(String s, int k, int start){
        
        if(k==0 || start == s.length()-1){
            //System.out.println(k);
            return;
        }
        
       
        char max = s.charAt(start); 
        for (int i = start; i < s.length(); i++) {
            char currentChar = s.charAt(i); 
            if (currentChar > max) {
                max = currentChar;
            }
        }
        
        
        for(int i=1; i<s.length(); i++){
            if(s.charAt(start) < s.charAt(i) && s.charAt(i)==max){
                s=swap(s,start,i);
                
                if(s.compareTo(ans)>0){
                    ans = s;
                }
                
                solve(s,k-1,start+1);
                s=swap(s,start,i);
            }
        }
        solve(s,k,start+1);
    }
    
    
    public static String findMaximumNum(String str, int k)
        {
            ans = str;
            solve(str,k,0);
            return ans;
        }
}
