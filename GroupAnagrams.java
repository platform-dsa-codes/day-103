import java.util.*;

class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        // Create a HashMap to store groups of anagrams
        Map<String, List<String>> map = new HashMap<>();
        
        // Iterate through each string in the array
        for (String str : strs) {
            // Convert the string to a character array and sort it
            char[] charArray = str.toCharArray();
            Arrays.sort(charArray);
            // Convert the sorted character array back to a string
            String sortedStr = new String(charArray);
            
            // Check if the sorted string is already in the map
            // If not, create a new list for it
            if (!map.containsKey(sortedStr)) {
                map.put(sortedStr, new ArrayList<>());
            }
            // Add the original string to the list associated with the sorted string
            map.get(sortedStr).add(str);
        }
        
        // Create a list to store the result
        List<List<String>> result = new ArrayList<>();
        
        // Add all values (lists of anagrams) from the map to the result list
        result.addAll(map.values());
        
        return result;
    }
}
